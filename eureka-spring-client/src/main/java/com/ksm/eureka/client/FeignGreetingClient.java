package com.ksm.eureka.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("eureka-greeting-service")
public interface FeignGreetingClient {
	
    @RequestMapping("/greeting")
    String greeting();
}


