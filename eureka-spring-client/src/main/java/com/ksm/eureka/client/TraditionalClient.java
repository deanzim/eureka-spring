package com.ksm.eureka.client;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

//@Controller
public class TraditionalClient {

//	@Autowired
	private EurekaClient eurekaClient;

	public String makeEurekaRestCall() {
		Application application = eurekaClient.getApplication("eureka-greeting-service");
		InstanceInfo instanceInfo = application.getInstances().get(0); 
		String hostname = instanceInfo.getHostName();
		int port = instanceInfo.getPort();
		System.out.println("Making call to " + hostname + ":" + port);
		String greeting = ""; 
		
		// ... make call to client
		
		return greeting;
	}
	
    @RequestMapping("/greeting")
    public String greeting(Model model) {
        model.addAttribute("greeting", makeEurekaRestCall());
        return "greeting-view";
    }


}
