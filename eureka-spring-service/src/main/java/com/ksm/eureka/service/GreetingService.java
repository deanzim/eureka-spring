package com.ksm.eureka.service;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.discovery.EurekaClient;

@SpringBootApplication
@RestController
public class GreetingService implements GreetingController {

	private static final Logger LOG = LoggerFactory.getLogger(GreetingService.class);

	@Autowired
	@Lazy
	private EurekaClient eurekaClient;

	@Value("${spring.application.name}")
	private String appName;

	public static void main(String[] args) {
		SpringApplication.run(GreetingService.class, args);
	}

	@Override
	public String greeting() {

		try {
			String name = eurekaClient.getApplication(appName).getName();
			String hostName = InetAddress.getLocalHost().getHostName();
			return String.format("Hello from '%s' running on host %s!", name, hostName);
		} catch (UnknownHostException e) {
			throw new RuntimeException("Error getting host information", e);
		}
	}
}